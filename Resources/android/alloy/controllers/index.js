function Controller() {
    function init() {
        footballService.getTeamInfo(function(teamData) {
            if (teamData) {
                $.testLabel.text = teamData.resultsCount;
                $.anotherTestLabel.text = teamData.headlines[0].headline;
                var tableData = [];
                Titanium.UI.createView({
                    top: 0,
                    backgroundColor: "#AB172D",
                    height: 70
                });
                for (var i = 0; 5 > i; i++) {
                    var row = Ti.UI.createTableViewRow({
                        rowIndex: i,
                        height: 70,
                        backgroundColor: "#FFFFFB"
                    });
                    var image = teamData.headlines[i].images[0];
                    var imageURL;
                    imageURL = "" == image || null == image ? "http://www.imms.org/images/dolphin_presentation/dolphin1.jpg" : teamData.headlines[i].images[0].url;
                    var imageAvatar = Ti.UI.createImageView({
                        image: imageURL,
                        left: 5,
                        right: 5,
                        top: 5,
                        width: 80,
                        height: 50
                    });
                    row.add(imageAvatar);
                    articleLink = teamData.headlines[i].links.mobile.href;
                    Ti.API.info(articleLink);
                    var eachHeadline = Ti.UI.createLabel({
                        color: "#474448",
                        font: {
                            fontFamily: "Arial",
                            fontSize: 15,
                            fontWeight: "bold"
                        },
                        text: teamData.headlines[i].headline,
                        left: 100,
                        top: 4,
                        bottom: 4,
                        height: 50,
                        id: "headline" + i,
                        headlineLink: articleLink
                    });
                    eachHeadline.addEventListener("click", function(e) {
                        Titanium.Platform.openURL(e.source.headlineLink);
                    });
                    row.add(eachHeadline);
                    var timeStamp = Ti.UI.createLabel({
                        color: "grey",
                        font: {
                            fontFamily: "Arial",
                            fontSize: 8,
                            fontWeight: "normal"
                        },
                        text: teamData.headlines[i].lastModified.substring(0, 10),
                        left: 20,
                        top: 60
                    });
                    row.add(timeStamp);
                    tableData.push(row);
                }
                var surveyRow = Ti.UI.createTableViewRow({
                    rowIndex: i,
                    height: 250,
                    backgroundColor: "#FFFFFB"
                });
                var shareRow = Ti.UI.createTableViewRow({
                    rowIndex: i + 1,
                    backgroundColor: "#70DBDB",
                    layout: "horizontal"
                });
                var loadview = Ti.UI.createView({
                    backgroundColor: "#70DBDB"
                });
                var webview = Ti.UI.createWebView({
                    url: "https://jenniferlong.wufoo.com/forms/z1cbnurp18vzz4i/",
                    top: -40,
                    showScrollbar: true,
                    backgroundColor: "#70DBDB"
                });
                var loadLabel = Ti.UI.createLabel({
                    text: "Loading, please wait...",
                    color: "#000"
                });
                surveyRow.add(webview);
                surveyRow.add(loadview);
                tableData.push(surveyRow);
                tableData.push(shareRow);
                var tableView = Ti.UI.createTableView({
                    backgroundColor: "#a6a6a6",
                    data: tableData,
                    top: 73
                });
                $.index.add(tableView);
            }
            webview.addEventListener("load", function() {
                loadview.hide();
                if ("https://jenniferlong.wufoo.com/forms/z1cbnurp18vzz4i/" != webview.url) {
                    webview.hide();
                    surveyRow.setHeight(0);
                }
            });
            webview.addEventListener("beforeload", function() {
                loadview.add(loadLabel);
            });
            var defaultNoImage = "http://www.freestockphotos.biz/pictures/4/4398/border.png";
            var btnShare = Ti.UI.createButton({
                Title: "Share",
                backgroundImage: "http://i.stack.imgur.com/P1ELC.png",
                font: {
                    size: 8,
                    color: "#000000"
                }
            });
            shareRow.add(btnShare);
            btnShare.addEventListener("click", function() {
                function getOrientation(o) {
                    switch (o) {
                      case Titanium.UI.PORTRAIT:
                        return "portrait";

                      case Titanium.UI.UPSIDE_PORTRAIT:
                        return "upside portrait";

                      case Titanium.UI.LANDSCAPE_LEFT:
                        return "landscape left";

                      case Titanium.UI.LANDSCAPE_RIGHT:
                        return "landscape right";

                      case Titanium.UI.FACE_UP:
                        return "face up";

                      case Titanium.UI.FACE_DOWN:
                        return "face down";

                      case Titanium.UI.UNKNOWN:
                        return "unknown";
                    }
                }
                var socialWin = Ti.UI.createWindow({});
                socialWin.open({
                    modal: true
                });
                var rowOne = Ti.UI.createView({
                    layout: "horizontal",
                    top: "0dip",
                    width: "100%"
                });
                socialWin.add(rowOne);
                var rowTwo = Ti.UI.createView({
                    layout: "horizontal",
                    top: "50dip",
                    width: "75%",
                    left: "12.5%"
                });
                socialWin.add(rowTwo);
                var rowThree = Ti.UI.createView({
                    layout: "horizontal",
                    top: "100dip"
                });
                socialWin.add(rowThree);
                var rowFour = Ti.UI.createView({
                    layout: "horizontal",
                    top: "355dip",
                    left: 0
                });
                socialWin.add(rowFour);
                var btnCancel = Ti.UI.createButton({
                    title: "Cancel",
                    height: "45dip"
                });
                rowOne.add(btnCancel);
                btnCancel.addEventListener("click", function() {
                    socialWin.close();
                });
                var shareTitle = Ti.UI.createLabel({
                    text: "Take a picture below to share!",
                    backgroundColor: "#FFFFFF",
                    width: "100%",
                    font: {
                        size: "100",
                        color: "#000000"
                    },
                    height: "45dip"
                });
                rowTwo.add(shareTitle);
                var takePicture = Ti.UI.createButton({
                    title: "Take Photo",
                    top: 0,
                    font: {
                        size: 8,
                        color: "#000000"
                    }
                });
                Ti.Gesture.addEventListener("orientationchange", function(e) {
                    Ti.API.info("Current orientation: " + orientationWhilePictureTaken + " (orientation = " + Ti.Gesture.orientation + ")");
                    var orientation = getOrientation(e.orientation);
                    Titanium.API.info("orientation changed = " + orientation + ", is portrait? " + e.source.isPortrait() + "(orientation = " + Ti.Gesture.orientation + ") is landscape? " + e.source.isLandscape());
                });
                takePicture.addEventListener("click", function() {
                    Titanium.Media.showCamera({
                        saveToPhotoGallery: true,
                        success: function(event) {
                            var orientationWhilePictureTaken = getOrientation(Titanium.Gesture.orientation);
                            Titanium.API.info("orientation: " + orientationWhilePictureTaken + " (orientation = " + Ti.Gesture.orientation + ")");
                            var fileName = "ps-" + new Date().getTime() + ".jpg";
                            var imageFile = Ti.Filesystem.getFile("file:///sdcard/").exists() ? Ti.Filesystem.getFile("file:///sdcard/", fileName) : Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, fileName);
                            imageFile.write(event.media);
                            if (event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
                                capturedImage.image = event.media;
                                if (orientationWhilePictureTaken = "portrait") {
                                    var matrix2d = Ti.UI.create2DMatrix();
                                    matrix2d = matrix2d.rotate(90);
                                    var spin = Ti.UI.createAnimation({
                                        transform: matrix2d,
                                        duration: 1e3,
                                        autoreverse: false,
                                        repeat: 0
                                    });
                                    capturedImage.animate(spin);
                                }
                            }
                        },
                        cancel: function() {},
                        error: function() {}
                    });
                });
                rowFour.add(takePicture);
                var removeImage = Ti.UI.createButton({
                    title: "Remove Photo"
                });
                removeImage.addEventListener("click", function() {
                    capturedImage.image = defaultNoImage;
                });
                rowFour.add(removeImage);
                var chkShare = Ti.UI.createSwitch({
                    titleOn: "Photo will post",
                    titleOff: "Photo won't post",
                    value: false
                });
                rowFour.add(chkShare);
                var capturedImage = Ti.UI.createImageView({
                    image: defaultNoImage,
                    top: "0",
                    left: "12.5%",
                    height: "250dip",
                    width: "250dip",
                    anchorPoint: {
                        x: .5,
                        y: .5
                    }
                });
                capturedImage.addEventListener("load", function() {
                    if (capturedImage.image == defaultNoImage) {
                        chkShare.value = false;
                        chkShare.enabled = false;
                    } else {
                        chkShare.value = true;
                        chkShare.enabled = true;
                    }
                });
                rowThree.add(capturedImage);
                var sendTextIntent = Ti.UI.createButton({
                    title: "Txt Int",
                    font: {
                        size: 8,
                        color: "#000000"
                    },
                    height: "45dip"
                });
                sendTextIntent.addEventListener("click", function() {
                    var intentText = Ti.Android.createIntent({
                        action: Ti.Android.ACTION_SEND,
                        type: "text/plain"
                    });
                    intentText.putExtra(Ti.Android.EXTRA_SUBJECT, "This is the subject.");
                    intentText.putExtra(Ti.Android.EXTRA_TEXT, "This is some text to send.");
                    Ti.Android.createIntentChooser(intentText, "Send Message");
                });
                rowOne.add(sendTextIntent);
                var sendImageIntent = Ti.UI.createButton({
                    title: "Img Int",
                    font: {
                        size: 8,
                        color: "#000000"
                    },
                    height: "45dip"
                });
                sendImageIntent.addEventListener("click", function() {
                    var intentImage = Ti.Android.createIntent({
                        type: "image/*",
                        action: Ti.Android.ACTION_PICK
                    });
                    intentImage.addCategory(Ti.Android.CATEGORY_DEFAULT);
                    Ti.Android.createIntentChooser(intentImage, "Share Photo");
                    var activity = socialWin.getActivity();
                    activity.startActivityForResult(intentImage, function(e) {
                        if (e.resultCode == Ti.Android.RESULT_OK) {
                            var Content = require("yy.ticontent");
                            var nativePath = e.intent.data;
                            nativePath = 0 === nativePath.indexOf("content://") ? "file://" + Content.resolveAudioPath(e.intentImage.data) : decodeURIComponent(nativePath);
                        }
                    });
                });
                rowOne.add(sendImageIntent);
            });
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.index = Ti.UI.createWindow({
        backgroundColor: "white",
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    $.__views.Heading = Ti.UI.createLabel({
        width: "100%",
        height: 70,
        color: "#2C3A5D",
        top: 0,
        backgroundColor: "#AB172D",
        textAlign: "center",
        font: {
            fontFamily: "Arial",
            fontSize: 40,
            fontWeight: "bold"
        },
        text: "Houston Texans",
        id: "Heading"
    });
    $.__views.index.add($.__views.Heading);
    $.__views.testLabel = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        id: "testLabel"
    });
    $.__views.index.add($.__views.testLabel);
    $.__views.anotherTestLabel = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        id: "anotherTestLabel"
    });
    $.__views.index.add($.__views.anotherTestLabel);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var footballService = require("sports");
    $.index.open();
    init();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;