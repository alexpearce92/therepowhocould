function parseJSON(responseText) {
    json = JSON.parse(responseText);
    Ti.API.debug(json);
    return json;
}

function onErrorHelper() {
    Ti.API.debug(e.error);
    alert("Could not retrieve data!");
}

function makeServiceCall(onSuccess, url) {
    var client = Ti.Network.createHTTPClient({
        onload: function() {
            var json = parseJSON(this.responseText);
            onSuccess(json);
        },
        onerror: function() {
            onErrorHelper();
        }
    });
    client.open("GET", url);
    client.send();
}

function getTeamInfo(onSuccess) {
    var url = "http://api.espn.com/v1/sports/football/nfl/news/?teams=34&apikey=gx4g3twpxt5xu8kt39yw59e9";
    Ti.API.info("Getting team info: " + url);
    makeServiceCall(onSuccess, url);
}

module.exports.parseJSON = parseJSON;

module.exports.onErrorHelper = onErrorHelper;

module.exports.makeServiceCall = makeServiceCall;

module.exports.getTeamInfo = getTeamInfo;